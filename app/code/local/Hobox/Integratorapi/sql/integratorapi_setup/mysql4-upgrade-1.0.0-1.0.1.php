<?php

/**
 * @category   HOBOX
 * @package    HOBOX_Integratorapi
 * @author     Rogeres Nascimento<rogeres19@gmail.com>
 * @company    HOBOX
 * @copyright (c) 2020, HOBOX
 * 
 */


$installer = $this;
$installer->startSetup();
$installer->run("
    CREATE TABLE `{$installer->getTable('integratorapi/catalog_product_integrator')}` (
      `integrator_product_id` int(11) NOT NULL auto_increment,
      `product_id` int(11),
      `product_sku` text NOT NULL,
      `status` varchar(11) NOT NULL,
      PRIMARY KEY  (`integrator_product_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;  ");
$installer->endSetup();