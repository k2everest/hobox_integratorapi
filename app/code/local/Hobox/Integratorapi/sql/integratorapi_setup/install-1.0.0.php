<?php
/**
 * @category Hobox
 * @package Hobox_Integratorapi
 * @author Rogeres Nascimento
 * @company Hobox
 * @copyright (c) 2017, Hobox
 *
 *
 * Hobox: PLataforma para integração de marketplaces
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$tableName = $this->getTable('sales/order');

// Order
if(!$installer->getConnection()->tableColumnExists($tableName, 'hobox_code')){
  $table_engine = $installer->getConnection()->raw_fetchRow("SHOW TABLE STATUS WHERE Name = '".$tableName."' ", 'Engine');
  if($table_engine == "InnoDB") {
    $installer->run("ALTER TABLE `{$tableName}` ADD `hobox_code` varchar(255) DEFAULT NULL");
    $installer->run('ALTER TABLE ' . $tableName . ' ADD UNIQUE (hobox_code);');
  }
}

$installer = new Mage_Sales_Model_Resource_Setup();
$installer->startSetup();
$table_engine = $installer->getConnection()->raw_fetchRow("SHOW TABLE STATUS WHERE Name = 'sales_flat_order'", 'Engine');
if($table_engine == "InnoDB") {
  $attributeParams = array('type' => Varien_Db_Ddl_Table::TYPE_VARCHAR);
  $installer->addAttribute("order", "hobox_code", $attributeParams);
  $installer->run("ALTER TABLE sales_flat_order ADD UNIQUE (hobox_code);");
}


$installer->endSetup();