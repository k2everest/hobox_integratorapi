<?php

/**
 * @category   Hobox
 * @package    Hobox_Integratorapi
 * @author     Rogeres Nascimento
 * @company    Hobox
 * @copyright (c) 2017, Hobox
 *
 *
  */

class Hobox_Integratorapi_Model_Tester_Api extends Mage_Api_Model_Resource_Abstract {


  public function version() {
      //MAJOR version when you make incompatible API changes
      //MINOR version when you add functionality in a backwards-compatible manner
      //PATCH version when you make backwards-compatible bug fixes
      return "1.0.0";
  }

  public function ping() {
     return "pong";
  }

}