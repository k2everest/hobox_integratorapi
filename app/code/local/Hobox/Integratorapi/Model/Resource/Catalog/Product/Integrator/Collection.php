<?php


class Hobox_Integratorapi_Model_Resource_Catalog_Product_Integrator_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract { 
    
    protected function _construct() {
            $this->_init('integratorapi/catalog_product_integrator');
    }
}