<?php

/**
 * @category   Hobox
 * @package    Hobox_Integratorapi
 * @author     Rogeres Nascimento
 * @company    Hobox
 * @copyright (c) 2017, Hobox
 *
 *
  */

class Hobox_Integratorapi_Model_Sale_Order_Api extends Mage_Sales_Model_Order_Api {

    const DEFAULT_HOBOX_GROUP_CODE = "HOBOX";

    private function applyDiscount($discountAmount, $quote, $discountDescription = 'Amount Waived'){
        $discountDescription =  Mage::helper("integratorapi")->__($discountDescription);
        if($discountAmount>0) {
            $total=$quote->getBaseSubtotal();
            $quote->setSubtotal(0);
            $quote->setBaseSubtotal(0);
 
            $quote->setSubtotalWithDiscount(0);
            $quote->setBaseSubtotalWithDiscount(0);
 
            $quote->setGrandTotal(0);
            $quote->setBaseGrandTotal(0); 
 
            $canAddItems = $quote->isVirtual()? ('billing') : ('shipping'); 
            foreach ($quote->getAllAddresses() as $address) {
 
                     $address->setSubtotal(0);
                     $address->setBaseSubtotal(0);
 
                     $address->setGrandTotal(0);
                     $address->setBaseGrandTotal(0);
 
                     $address->collectTotals();
 
                     $quote->setSubtotal((float) $quote->getSubtotal() + $address->getSubtotal());
                     $quote->setBaseSubtotal((float) $quote->getBaseSubtotal() + $address->getBaseSubtotal());
 
                     $quote->setSubtotalWithDiscount(
                         (float) $quote->getSubtotalWithDiscount() + $address->getSubtotalWithDiscount()
                     );
                     $quote->setBaseSubtotalWithDiscount(
                         (float) $quote->getBaseSubtotalWithDiscount() + $address->getBaseSubtotalWithDiscount()
                     );
 
                     $quote->setGrandTotal((float) $quote->getGrandTotal() + $address->getGrandTotal());
                     $quote->setBaseGrandTotal((float) $quote->getBaseGrandTotal() + $address->getBaseGrandTotal());
 
                     $quote->save(); 
 
                    $quote->setGrandTotal($quote->getBaseSubtotal()-$discountAmount)
                    ->setBaseGrandTotal($quote->getBaseSubtotal()-$discountAmount)
                    ->setSubtotalWithDiscount($quote->getBaseSubtotal()-$discountAmount)
                    ->setBaseSubtotalWithDiscount($quote->getBaseSubtotal()-$discountAmount)
                    ->save(); 
 
                    if($address->getAddressType()==$canAddItems) {
        
                        $address->setSubtotalWithDiscount((float) $address->getSubtotalWithDiscount()-$discountAmount);
                        $address->setGrandTotal((float) $address->getGrandTotal()-$discountAmount);
                        $address->setBaseSubtotalWithDiscount((float) $address->getBaseSubtotalWithDiscount()-$discountAmount);
                        $address->setBaseGrandTotal((float) $address->getBaseGrandTotal()-$discountAmount);
                        if($address->getDiscountDescription()){
                            $address->setDiscountAmount(-($address->getDiscountAmount()-$discountAmount));
                            $address->setDiscountDescription($address->getDiscountDescription().','.$discountDescription);
                            $address->setBaseDiscountAmount(-($address->getBaseDiscountAmount()-$discountAmount));
                        }else {
                            $address->setDiscountAmount(-($discountAmount));
                            $address->setDiscountDescription($discountDescription);
                            $address->setBaseDiscountAmount(-($discountAmount));
                        }
                        $address->save();
                    }//end: if
            } //end: foreach
 
            foreach($quote->getAllItems() as $item){
                //We apply discount amount based on the ratio between the GrandTotal and the RowTotal
                $rat=$item->getPriceInclTax()/$total;
                $ratdisc=$discountAmount*$rat;
                $item->setDiscountAmount(($item->getDiscountAmount()+$ratdisc) * $item->getQty());
                $item->setBaseDiscountAmount(($item->getBaseDiscountAmount()+$ratdisc) * $item->getQty())->save();

            }
        }
    }

    public function getIncrementId($hobox_code) {

        $result = array();
        try {
            $orderExists = Mage::getModel("sales/order")->loadByAttribute("hobox_code", $hobox_code);
            
            if ($orderExists->getId()) {
                $result[0] = 'success';
                $result[1] = $orderExists->getIncrementId();
            }else
               $result[0] = 'Not Found'; 
        } catch (Exception $e) {
            $result[]  = 'Error: ' . $e;
    
        }

        return $result;
    }

    public function create($storeCode ='default', $order) {
        
        $order = json_decode($order);
        $order->shipping_description = str_replace(' ','-',$order->shipping_description);
        $errors = array();
        try {
            $orderExists = Mage::getModel("sales/order")->loadByAttribute("hobox_code", $order->hobox_code);

            if ($orderExists->getId()) {
                $errors[] = "order already imported";
                return $errors;
            }

            $customerGroup = Mage::getModel('customer/group')->load(self::DEFAULT_HOBOX_GROUP_CODE, "customer_group_code");
            if (!$customerGroup->getId()) {
                $notLoggedInGroup = Mage::getModel('customer/group')->load(Mage_Customer_Model_Group::NOT_LOGGED_IN_ID);
                $customerGroup->setCode(self::DEFAULT_HOBOX_GROUP_CODE);
                $customerGroup->setTaxClassId($notLoggedInGroup->getTaxClassId());
                $customerGroup->save();
            }

            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $currentStoreId = Mage::app()->getStore($storeCode)->getId();
            $quote = Mage::getModel('sales/quote')->setStoreId($currentStoreId);
            $quote->setIsSuperMode(true);
            $quote->setReservedOrderId(Mage::getSingleton('eav/config')->getEntityType('order')->fetchNewIncrementId($currentStoreId));
            $quote->setHoboxCode($order->hobox_code);
            $this->prepareShippingMethod($order);

            $this->addItemsToQuote($quote, $order->items);

            $billingAddress = $this->parseAddress($order->billing_address, $order->customer_email);
            $shippingAddress = $this->parseAddress($order->shipping_address, $order->customer_email);
            
            $quote->getBillingAddress()->addData($billingAddress);
            $quote->getShippingAddress()->addData($shippingAddress);
            $quote->getShippingAddress()->setCollectShippingRates(true);
            $quote->getShippingAddress()->collectShippingRates();
            $quote->getShippingAddress()->setShippingMethod('hobox'.'_'.$order->shipping_description);
            //$quote->setPaymentMethod('hobox_payment');
            //$quote->getShippingAddress()->collectShippingRates();
            //$tes = $quote->getShippingAddress()->collectShippingRates();
            //Mage::log($tes,null,'hoboxTest.log');          
            //$quote->collectTotals();
            //Mage::log($quote,null,'hoboxQuote5.log');
            /* >setCollectShippingRates(true)
                ->collectShippingRates()
                ->collectTotals()
                ->setShippingMethod('hobox_hobox'); */
                //->setPaymentMethod('hobox_payment');


            $quote->setCheckoutMethod('guest')
                        ->setCustomerId(null)
                        ->setCustomerFirstname($order->customer_firstname)
                        ->setCustomerLastname($order->customer_lastname)
                        ->setCustomerDob($order->customer_dob)
                        ->setCustomerTaxvat($order->billing_address->vat_number)
                        ->setCustomerCpf($order->billing_address->vat_number)
                        ->setCpf($order->billing_address->vat_number)
                        ->setCustomerEmail($quote->getBillingAddress()->getEmail())
                        ->setCustomerIsGuest(true)
                        ->setCustomerGroupId($customerGroup->getId());

            $quote->getPayment()->importData( array('method' => 'hobox_payment'));
            $discountAmount = (float) $quote->getSubtotal() - $order->total;
            
            $quote->save();
            if($discountAmount > 0){
                $this->applyDiscount($discountAmount,$quote);
            }
            if((float) $order->free_shipping_discount > 0){
                $description  = $order->rule_offset_freight ? 'Built-in Free Shipping' : 'Free Shipping';
                $this->applyDiscount($order->free_shipping_discount,$quote,$description);
            }

            $service = Mage::getModel('sales/service_quote', $quote);
            $service->submitAll();

            $mage_order = $service->getOrder();
            $mage_order->addStatusHistoryComment('Hobox code: ' . $order->hobox_code, $mage_order->getStatus());
            $mage_order->save();


        } catch (Exception $e) {
            $errors[]  = 'Error: ' . $e;

        }

        return $errors;
    }

    public function listShipments($filters = null) {

        $collection = Mage::getResourceModel('sales/order_shipment_collection')
            ->addAttributeToSelect('increment_id')
            ->addAttributeToSelect('created_at')
            ->addAttributeToSelect('total_qty')
            ->join('sales/order', 'order_id=`sales/order`.entity_id', array('skyhub_code' => 'skyhub_code'), null, 'left');

        $preparedFilters = array();
        if (isset($filters->filter)) {
            foreach ($filters->filter as $_filter) {
                $preparedFilters[$_filter->key] = $_filter->value;
            }
        }
        if (isset($filters->complex_filter)) {
            foreach ($filters->complex_filter as $_filter) {
                $_value = $_filter->value;
                $preparedFilters[$_filter->key] = array(
                    $_value->key => $_value->value
                );
            }
        }

        if (!empty($preparedFilters)) {
            try {
                foreach ($preparedFilters as $field => $value) {
                    if (isset($this->_attributesMap['shipment'][$field])) {
                        $field = $this->_attributesMap['shipment'][$field];
                    }

                    $collection->addFieldToFilter($field, $value);
                }
            } catch (Mage_Core_Exception $e) {
                $this->_fault('filters_invalid', $e->getMessage());
            }
        }


        $result = array();

        foreach ($collection as $shipment) {
            $result[] = $this->_getAttributes($shipment, 'shipment');
        }

        return $result;
    }

    public function invoice($hoboxCode, $newStatus) {
        $errors = array();
        try {
            $order = Mage::getModel("sales/order")->loadByAttribute("hobox_code", $hoboxCode);
            if (!$order->getId()) {
                $errors[] = "Não foi possível criar a fatura: Pedido(" . $hoboxCode.  ")   não encontrado";
                return $errors;
            }

            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);

            if(count($newStatus) > 0) {
                $invoice->getOrder()->setState(Mage_Sales_Model_Order::STATE_PROCESSING, $newStatus);
            }else {
                $invoice->getOrder()->setState(Mage_Sales_Model_Order::STATE_PROCESSING);
            }

            $invoice->register();
            $transactionSave = Mage::getModel('core/resource_transaction')
                ->addObject($invoice)
                ->addObject($invoice->getOrder());
            $transactionSave->save();
        } catch (Exception $e) {
         $errors[]  = 'Error: ' . $e;
        }
        return $errors;

    }

    public function Cancel($hoboxCode)
	{	

		try
		{

            $order = Mage::getModel("sales/order")->loadByAttribute("hobox_code", $hoboxCode);

			if (is_null($order))
			{
				Mage::helper("integratorapi")->log('order not found for orderCancel');
				return "order not found";
            }
            
            $invoices = $order->getInvoiceCollection();
                foreach ($invoices as $invoice){
                $invoice->delete();
            }


			/* if(!$order->canCancel())
			{
				Mage::helper("integratorapi")->log('order cannot cancel for orderCancel');
				return "cant cancel";
			} */

			$order->cancel();
			$order->setStatus("canceled");
			$order->setState("canceled");
			$history = $order->addStatusHistoryComment('Cancel. - Hobox', false);
			$history->setIsCustomerNotified(false);

			$order->save();

			// Finished
			return "canceled";
		}
		catch (Exception $e)
		{
			Mage::logException($e);
			return "Error: ".$response.Mage::helper("integratorapi")->MakePrettyException($e);
		}
	}


    public function update($order) {
        $errors = array();
        try {
            $magentoOrder = Mage::getModel("sales/order")->loadByAttribute("hobox_code", $order->hobox_code);
            if (!$magentoOrder->getId()) {
                $errors[] = "Pedido com hobox code " . $order->hobox_code . " não foi encontrado";
                return $errors;
            }

            if($order->billing_address){
              $billingAddress = $this->parseAddress($order->billing_address, $order->customer_email);
              $magentoOrder->getBillingAddress()->addData($billingAddress)->implodeStreetAddress()->save();
            }

            if($order->shipping_address){
              $shippingAddress = $this->parseAddress($order->shipping_address, $order->customer_email);
              $magentoOrder->getShippingAddress()->addData($shippingAddress)->implodeStreetAddress()->save();
            }

        } catch (Exception $e) {
            $errors[]  = 'Error: ' . $e;
        }
        return $errors;
    }


    public function prepareShippingMethod($orderData){
        $sessionId = Mage::getSingleton('api/session')->getSessionId();
        if (Mage::registry('customShippingInfo' . $sessionId) != NULL)
            Mage::unregister('customShippingInfo' . $sessionId);
        $shippingPrice = $orderData->shipping_seller_cost;
        Mage::register('customShippingInfo' . $sessionId, array('provider' => 'hobox', 'service'=> $orderData->shipping_description, 'price' => $shippingPrice));
        //Mage::register('customShippingInfo' . $sessionId, array('provider' => 'hobox', 'service' => 'hobox', 'price' => $orderData->shipping_amount));
        //Mage::log($sessionId,null,'hoboxFinal.log');
    }

    public function addItemsToQuote($quote, $itemsData) {
        $itemsArray = (array)$itemsData;
        foreach($itemsArray as $itemEntry) {
            if (is_array($itemEntry)) {
               $item = $itemEntry[0];
            }else {
               $item = $itemEntry;
            }
            $id = Mage::getModel('catalog/product')->getIdBySku($item->sku);
            $product = Mage::getModel('catalog/product')->load($id);
            $product->setSkipCheckRequiredOption(true);
            $product['sku'] = $item->sku;
            $product['price'] = $item->original_price;
            $product['special_price'] = $item->special_price;
            $buyInfo = array('qty' => $item->qty);
            $quoteItem = $quote->addProduct($product, new Varien_Object($buyInfo));
            $product->unsSkipCheckRequiredOption();

        }
    }

    public function getNfeBling($increment_id){

        if($this->isBlingInstalled()){
            $blingNfeCollection = Mage::getModel("blingmagento/nfe")->getCollection()->addFieldToFilter('increment_id',$increment_id);
            if($blingNfeCollection->count() == 0){
                return null;
            }
            else{
                return $blingNfeCollection->getFirstItem();
            }
        }
        else
            return null;
    }

    public function getNfeXmlBling($documentNumber,$documentSerie){
        $apikey =  Mage::getStoreConfig('nfe/general/chave_acesso_bing');
        $outputType = "json";
        $url = 'https://bling.com.br/Api/v2/notafiscal/' . $documentNumber . '/'. $documentSerie . '/' . $outputType;
        $retorno = $this->executeGetFiscalDocument($url, $apikey);
        return $retorno;

    }

    private  function executeGetFiscalDocument($url, $apikey){
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url . '&apikey=' . $apikey);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($curl_handle);
        curl_close($curl_handle);
        return $response;
    }


    public function getOrderInfo($incrementId){
        $bling = $this->getNfeBling($incrementId);
        $keyNfe ="";
        $numberNfe = "";
        if($bling!=null){
            $keyNfe = $bling->getNfKey();
            $numberNfe = $bling->getNfNumber();
            $seriesNfe = $bling->getNfSeries();
            $response = $this->getNfeXmlBling($numberNfe, $seriesNfe);
            $response = json_decode($response);
            Mage::log($response,null,'hobox_api_nfe.log');
            $nfeData = $response->retorno->notasfiscais[0]->notafiscal;
            $result= array();
            $nfe = array();
            $nfe['serie']=$nfeData->serie;
            $nfe['numero']=$nfeData->numero;
            $nfe['key']=$nfeData->chaveAcesso;
            $nfe['xml']=$nfeData->xml;
            $result['nfe'] = $nfe;
            return $result;
        }

        return null;

    }

    private function isBlingInstalled(){
        $modules = Mage::getConfig()->getNode('modules')->children();
        $modulesArray = (array)$modules;

        if(isset($modulesArray['K2everest_Blingmagento']) && (string) $modulesArray['K2everest_Blingmagento']->active == 'true') {
            Mage::log('bling instalado',null,'hobox.log');
            return true;        
        } else{
            Mage::log('bling nao instalado',null,'hobox.log');
            return false;
        }
    }

     public function parseAddress($addressData, $customerEmail) {
        $directory = Mage::getModel("directory/region")->loadByCode($addressData->region, $addressData->country_id);
        if(!$addressData->secondary_phone){
            $addressData->secondary_phone = $addressData->telephone;
        }
        $address = array(
            'firstname' => strlen(trim(preg_replace('/\s+/', ' ',  $addressData->firstname ))) > 0 ? $addressData->firstname : $addressData->lastname,
            'lastname'  => $addressData->lastname,
            'middlename' => '',
            'company'   => $addressData->company,
            'email'     =>  $customerEmail,
            'street' => array(
              '0' => $addressData->street,
              '1' => $addressData->number,
              '2' => $addressData->complement,
              '3' => $addressData->neighborhood
            ),
            'complemento' => $addressData->complement,
            'bairro' => $addressData->neighborhood,
            'number' => $addressData->number,
            'city' => $addressData->city,
            'region_id' => $directory->getRegionId(),
            'region' => $addressData->region,
            'postcode' => $addressData->postcode,
            'country_id' => $addressData->country_id,
            'telephone' =>  $addressData->telephone,
            'celular'   => $addressData->secondary_phone,
            'fax' => $addressData->fax,
            'customer_password' => '',
            'vat_id' => $addressData->vat_number,
            'cpf' => $addressData->vat_number,
            'confirm_password' =>  '',
            'save_in_address_book' => '0',
            'use_for_shipping' => '1',
            ''
        );
        return $address;
    }

   /* public function parseAddress($addressData, $customerEmail) {
        $addressData->region = 'N/A';
        $directory = Mage::getModel("directory/region")->loadByCode($addressData->region, $addressData->country_id);
        if(!$addressData->secondary_phone){
            $addressData->secondary_phone = $addressData->telephone;
        }
        $address = array(
            'firstname' => 'rogeres',
            'lastname'  => 'nascimento',
            'company'   => 'teste',
            'email'     =>  'rogeres19@gmail.com',
            'street' =>  array(
                '0' => 'rua nova',
                '1' =>  'teste',
                '2' => 'teste',
                '3' => 'teste'
            ),
            'complemento' => '',
            'bairro' => 'centro',
            'number' => 19,
            'city' => 'ibirataia',
            'region_id' => $directory->getRegionId(),
            'region' => 'BA',
            'postcode' => 45580000,
            'country_id' => $addressData->country_id,
            'telephone' =>  '7199829095',
            'celular'   => '7199829095',
            'fax' => $addressData->fax,
            'customer_password' => '',
            'vat_id' => '04749378526',
            'cpf' => '04749378526',
            'confirm_password' =>  '',
            'save_in_address_book' => '0',
            'use_for_shipping' => '1',
            ''
        );
        return $address;
    }
    */

    public function paymentMethods() {
       $payments = Mage::getSingleton('payment/config')->getAllMethods();
       $methods = array();
       foreach ($payments as $paymentCode=>$paymentModel) {
            $paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
            $methods[] = array( 'key' => $paymentCode, 'value' => $paymentTitle);
        }
        return $methods;
    }

    public function shippingMethods() {
       $methods = Mage::getSingleton('shipping/config')->getAllCarriers();
        $shipMethods = array();
        foreach ($methods as $shippigCode=>$shippingModel) {
            $shippingTitle = Mage::getStoreConfig('carriers/'.$shippigCode.'/title');
            $shipMethods[] = array( 'key' => $shippigCode, 'value' => $shippingTitle);
        }
        return $shipMethods;
    }


    public function listStatus(){
        $collection = Mage::getResourceModel('sales/order_status_collection');
        if($collection == null) {
            return $this->listStatusMagento14();
        }
        $collection->joinStates();
        $result = array();
        foreach($collection as $status) {
           $result[] = array( 'status' => $status->getStatus(), 'status_label' => $status->getLabel(), 'state' => $status->getState());
        }
        return $result;
    }

    public function listStatusMagento14() {
        $result = array();
        $result[] = array( 'status' => "pending", 'status_label' => "Pending", 'state' => "new");
        $result[] = array( 'status' => "pending_payment", 'status_label' => "Pending Payment", 'state' => "pending_payment");
        $result[] = array( 'status' => "processing", 'status_label' => "Processing", 'state' => "processing");
        $result[] = array( 'status' => "holded", 'status_label' => "On Hold", 'state' => "holded");
        $result[] = array( 'status' => "complete", 'status_label' => "Complete", 'state' => "complete");
        $result[] = array( 'status' => "closed", 'status_label' => "Closed", 'state' => "closed");
        $result[] = array( 'status' => "canceled", 'status_label' => "Canceled", 'state' => "canceled");
        $result[] = array( 'status' => "fraud", 'status_label' => "Suspected Fraud", 'state' => "payment_review");
        $result[] = array( 'status' => "payment_review", 'status_label' => "Payment Review", 'state' => "payment_review");
        return $result;
    }



}
