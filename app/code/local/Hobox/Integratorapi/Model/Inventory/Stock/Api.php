<?php

/**
 * Catalog inventory api
 *
 * @category   Hobox
 * @package    Hobox_Integratorapi
 * @author     Rogeres Nascimento <rogeres19@gmail.com>
 */
class Hobox_Integratorapi_Model_Inventory_Stock_Api extends Mage_CatalogInventory_Model_Stock_Item_Api
{

    public function listStock($productIds) {
        
        if (!is_array($productIds)) {
            $productIds = array($productIds);
        }

        $product = Mage::getModel('catalog/product');

        foreach ($productIds as &$productId) {
            if ($newId = $product->getIdBySku($productId)) {
                $productId = $newId;
            }
        }

        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->setFlag('require_stock_items', true)
            ->addFieldToFilter('entity_id', array('in'=>$productIds));

        $result = array();

        foreach ($collection as $product) {
            $qty = false;
            if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE)
                $qty = $this->getBundleStock($product);
            if ($product->getStockItem()) {
                if($qty==false)
                    $qty = $product->getStockItem()->getQty();
                
                $result[] = array(
                    'product_id'    => $product->getId(),
                    'sku'           => $product->getSku(),
                    'qty'           => $qty,
                    'is_in_stock'   => $product->getStockItem()->getIsInStock()
                );
            }
        }

        return $result;
    }

    private function getBundleStock($_product){
        $selectionCollection = $_product->getTypeInstance()->getSelectionsCollection($_product->getTypeInstance()->getOptionsIds());
        $qty = false;
        foreach ($selectionCollection as $option) {
            $product_id = $option->product_id;
            $bundleOption = Mage::getModel('catalog/product')->load($product_id);
            $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($bundleOption);
            if ($qty === false) {
                $qty = $stock->getQty();
            } else {
                $qty = min(array($qty, $stock->getQty()));
            }
        }

        return $qty;

    }

    
}
 // Class Mage_CatalogInventory_Model_Stock_Item_Api End
