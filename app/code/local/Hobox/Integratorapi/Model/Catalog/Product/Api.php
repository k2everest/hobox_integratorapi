<?php

/**
 * @category   Hobox
 * @package    Hobox_Integratorapi
 * @author     Rogeres Nascimento
 * @company    Hobox
 * @copyright (c) 2017, Hobox
 *
 *
  */
class Hobox_Integratorapi_Model_Catalog_Product_Api extends Mage_Catalog_Model_Product_Api {

   public function listAllAttributes(){
        $attributes = Mage::getResourceModel('catalog/product_attribute_collection')
        ->getItems();
        $listAttributes = array();


         foreach ($attributes as $attribute){
             $options = null;
            if ($attribute->usesSource()) {
                $options = $attribute->getSource()->getAllOptions(false);
            }
           $listAttributes[] = array(
                "name"=> $attribute->getAttributecode(),
                "label"=> $attribute->getFrontendLabel(),
                "options"=> $options
           );
        }
        return $listAttributes;

   }

   public function listItems($storeCode = null, $updated_from = null, $attribute_set = null,$page=0, $limit_page = 50) {
        if($updated_from){
            $updated_from = Mage::getModel('core/date')->timestamp(strtotime($updated_from));
            $updated_from = date('Y-m-d H:i:s', $updated_from);
       }
       Mage::helper("integratorapi")->log('ListItems: UpdateFrom:' . $updated_from );
       $collection = $this->_itemsCollection($storeCode, $updated_from, $attribute_set);
       $totalItems = $collection->getSize();
       $collection->setCurPage($page)->setPageSize($limit_page);
       $products = $this->_addMediaGalleryToProducts($collection, 'configurable_based_price');
       $result = array('total'=>$totalItems,'page'=>$page, 'limit'=>$limit_page,'products'=>$products);

       return $result;
   }

   protected function _addMediaGalleryToProducts($productCollection, $getVariationPrices) {
       $result = array();
       $productsIds = array();

        foreach ($productCollection as $product) {
            array_push($productsIds,$product->getId());
            $product->load('media_gallery');
            $product_data = $this->_toArray($product, $getVariationPrices);

            $exportConfigurableWithoutVariations = Mage::getStoreConfig('integratorapi/catalog/export_configurable_without_variations');

            if ($exportConfigurableWithoutVariations || !$this->_configurableWithoutEnabledVariations($product_data)) {
              $result[] = $this->_toArray($product, $getVariationPrices);
            }
        }

        Mage::helper("integratorapi")->log('Updated/Inserted Products total: '.count($productsIds).' ids: '.implode(",",$productsIds));

        return $result;
   }

   protected function _configurableWithoutEnabledVariations($product_data) {
       return array_key_exists('configurable_attributes', $product_data) &&
               count($product_data['configurable_attributes']) > 0 &&
                count($product_data['variations']) == 0;
   }

    protected function _setCurrentStoreCode($storeCode = null){
        if($storeCode){
            $currentStoreCode = $storeCode;
            Mage::app()->setCurrentStore($storeCode);
        } else{
            $catalogWebstoreConfig = Mage::getStoreConfig('integratorapi/catalog/catalog_webstore');
            if($catalogWebstoreConfig){
              $currentStoreCode = $catalogWebstoreConfig;
              Mage::app()->setCurrentStore($catalogWebstoreConfig);
            }
        }
        return $currentStoreCode;
    }

   protected function _itemsCollection($storeCode = null, $updated_from = null, $attribute_set = null) {
       $currentStoreCode = $this->_setCurrentStoreCode($storeCode);

       $visibilities = Mage::getStoreConfig('integratorapi/catalog/visibilities_to_export');
       Mage::helper("integratorapi")->log('visibilities: '.$visibilities);

       $collection = Mage::getModel("catalog/product")->getCollection()
                     ->addAttributeToFilter('visibility', array('in' => explode(',', $visibilities)))
                     ->addAttributeToSelect("*");

       if (!Mage::getStoreConfig('integratorapi/catalog/export_disabled')) {
           $collection->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));
       }


       if ($currentStoreCode) {
           $collection->addStoreFilter();
       }
       if ($attribute_set && strlen($attribute_set) > 0) {
            $collection->addAttributeToFilter('attribute_set_id', $attribute_set);
       }
       if ($updated_from && strlen($updated_from) > 0) {
            $collection->addAttributeToFilter('updated_at', array('gteq' => $updated_from));
       }

       $collection->addUrlRewrite();
       return $collection;
   }


    function utf8_for_xml($string) {
        return preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
    }

    /**
     *
     *
     * @param type $product
     * @return type
     */
    protected function _toArray($product, $getVariationPrices) {
        $multiselectAttributes = array();
        $attributes[] = array( 'key' => 'product_id', 'value' => $product->getId());
        $attributes[] = array( 'key' => 'sku', 'value' => $product->getSku());
        $attributes[] = array( 'key' => 'set', 'value'        => $product->getAttributeSetId());
        $attributes[] = array( 'key' => 'type', 'value'       => $product->getTypeId());
        $attributes[] = array( 'key' => 'categories', 'value' =>  $this->implodeIfArray($product->getCategoryIds()));
        $attributes[] = array( 'key' => 'websites', 'value'   => $this->implodeIfArray($product->getWebsiteIds()));

        foreach ($product->getTypeInstance(true)->getEditableAttributes($product) as $attribute) {
            if ($this->_isAllowedAttribute($attribute)) {
                if($attribute->getFrontendInput() == "multiselect") {
                    $multiselectAttributes[] = Array ("key" => $attribute->getAttributeCode(), "value" => $this->implodeIfArray($product->getData($attribute->getAttributeCode())));
                } else {
                  $entry =  Array ("key" => $attribute->getAttributeCode(), "value" => $this->utf8_for_xml($this->implodeIfArray($product->getData($attribute->getAttributeCode()))) );
                  if($entry['key'] == 'special_price' || ($entry['key'] == 'price' && $this->_isBundle($product))) {
                      $entry['value'] = Mage::helper("integratorapi")->getFinalPrice($product);
                  }
                  $attributes[] = $entry;
                }

            }
        }

        $result['image_urls'] = array();
        foreach($product->getMediaGalleryImages() as $image) {
          if($product->getImage() && strpos($image->getUrl(), $product->getImage()) !== false) {
              array_unshift($result['image_urls'], $image->getUrl());
          } else {
            $result['image_urls'][] = $image->getUrl();
          }
        }

        $result['attributes'] = $attributes;
        if (count($multiselectAttributes) > 0 ) {
            $result['multiselect_attributes'] = $multiselectAttributes;
        }
        if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
            $result['configurable_attributes'] = Mage::helper("integratorapi")->getConfigurableAttributes($product);
            $result["variations"] = Mage::helper("integratorapi")->getConfigurableProductVariation($product, $getVariationPrices);
        } elseif ($this->_isBundle($product)) {
            $result["bundle_components"] = Mage::helper("integratorapi")->getBundleComponents($product);
        }


        $result['full_url'] = $product->getProductUrl();

        return  $result;
    }

    protected function getOptionValue($attribute,$product){
        $initial = $product->getData($attribute->getAttributeCode());
        $value = null;
        if ($attribute->usesSource()) {

            if(is_array($initial)){
                foreach ($attribute->getSource()->getAllOptions() as $optionId => $optionValue) {
                    if (is_array($optionValue)){
                        $options[] = $optionValue['label'];
                        if(in_array($optionValue['value'],$initial))
                            $value[] = $optionValue['label'];
                    }
                }
            }else{
                foreach ($attribute->getSource()->getAllOptions() as $optionId => $optionValue) {
                    if (is_array($optionValue)){
                        $options[] = $optionValue['label'];
                        if($optionValue['value'] == $initial)
                            $value = $optionValue['label'];
                    }
                }
            }
        }

        return $value == null ?  $initial : $value;

    }

 /*    protected function hasOptions($attribute){
        $options = array();
        if ($attribute->usesSource()) {
            foreach ($attribute->getSource()->getAllOptions() as $optionId => $optionValue) {
                if (is_array($optionValue))
                    $options[] = $optionValue['label'];
            }
        }
        return $options;
    } */

    protected function _isBundle($product){
      return $product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE;
    }

    protected function _getFinalPrice($product) {
        if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){
          return Mage::helper("integratorapi")->getBundleFinalPrice($product);
        }
         $observer = new Varien_Event_Observer();
         $product->getFinalPrice();
         $event = new Varien_Event(array('product'=>$product, 'qty' => 1));
         $observer->setData(array('event'=> $event));
         Mage::getModel("catalogrule/observer")->processFrontFinalPrice($observer);
         return $product->getData("final_price");
    }

    public function implodeIfArray($data) {
        if (is_array($data)) {
           return implode(' ; ', $data);
        } else {
            return $data;
        }
    }

    protected function _getProductIntegratorCollection($status, $curPage = null, $pageSize = null) {
        $collection = Mage::getModel("integratorapi/catalog_product_integrator")->getCollection()
                       ->addFieldToFilter('status', array('eq' => $status));
        if ($curPage && $pageSize) {
            $collection->setPageSize($pageSize)->setCurPage($curPage);
        }

        return $collection;
    }

    public function listDeletedItems() {
        $collection = $this->_getProductIntegratorCollection(Hobox_Integratorapi_Model_Status::DELETED);
        $idsToReturn = array();
        foreach ($collection as $product_integrator) {
            if ($product_integrator->getProductId()) {
                $idsToReturn[]= $product_integrator->getProductId();
            }
        }
        return $idsToReturn;
    }


}
