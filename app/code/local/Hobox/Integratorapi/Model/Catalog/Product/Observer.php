<?php

class Hobox_Integratorapi_Model_Catalog_Product_Observer {
    

    public function catalog_product_delete_after($observer) {
        $product = $observer->getProduct();
        if (!Mage::getModel("integratorapi/catalog_product_filter")->isProductTypeValid($product)) {
            return;
        }
        $integratorProduct = Mage::getModel("integratorapi/catalog_product_integrator")->loadByProductData($product);        
        $integratorProduct->setProductData($product, Hobox_Integratorapi_Model_Status::DELETED);
        $integratorProduct->save();
        
    }
}