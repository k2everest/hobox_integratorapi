<?php


/**
 * @category   Hobox
 * @package    Hobox_Integratorapi
 * @author     Rogeres Nascimento
 * @company    Hobox
 * @copyright (c) 2017, Hobox
 *
 *
  */
/*
 *
 * To avoid those problems, it is necessary to have a special shipping method that can be used
 * only by this module.
 */

class Hobox_Integratorshipping_Model_Carrier  extends Mage_Shipping_Model_Carrier_Abstract
implements Mage_Shipping_Model_Carrier_Interface
{

    protected $_code = 'hobox';

    public function getAllowedMethods()
	{   
      $shippingMethod = Mage::registry('customShippingInfo' . Mage::getSingleton('api/session')->getSessionId());
      Mage::log($shippingMethod,null,'hoboxShippingMethod.log');
      if ($this->isAPI() && $shippingMethod['service'] != 0)
         return array($shippingMethod['service'] => $shippingMethod['service']);
      
      return array(); //none allowed
	}
	
	protected function isAPI()
	{
		return Mage::getSingleton('api/session')->getSessionId() != "";
	}
	
	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
   {
      if (!$this->getConfigFlag('active')) {
         Mage::log('HoboxCarrier not Active');
         return false;
      }

      if (!$this->isAPI()) {
         Mage::log('HoboxCarrier not available for not API');
			return false;
		}

      $sessionId = Mage::getSingleton('api/session')->getSessionId();
      $customShipping = Mage::registry('customShippingInfo' . $sessionId);
            
		$method = Mage::getModel('shipping/rate_result_method'); //new instance
      
      //Setting Carrier/Provider
      $method->setCarrier($customShipping['provider']);
      $method->setCarrierTitle('Hobox');
      
      //Setting Method/Service
      $method->setMethod($customShipping['service']);
      $method->setMethodTitle($customShipping['service']);
      
      //Setting Price
      if (isset($customShipping['price'])) {
         $method->setCost($customShipping['price']);
         $method->setPrice($customShipping['price']);
      }
      
      $result = Mage::getModel('shipping/rate_result');
		$result->append($method);
		
		return $result;
	}
}
?>