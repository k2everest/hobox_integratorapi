<?php
/**
 * @category   Hobox
 * @package    Hobox_Integratorapi
 * @author     Rogeres Nascimento
 * @company    Hobox
 * @copyright (c) 2017, Hobox
 *
 *
 *
 * The purpose of this class is to serve as a message class.
 *
 * !!!IMPORTANT!!!!
 * ########################################################################
 * This  class must be used as a SINGLETON class. This means that always
 * to get an objection of this class, the following magento method MUST be used:
 *
 * $shippingConfiguration = Mage::getSingleton("integratorapi/sale_shipping_configuration")
 *
 *
 * If these values are not set properly, Magento will rise an exception and
 * the order will not be imported.
 *
 *
 * The parameters that must be set are listed bellow.
 *
 * Shipping Configuration Model
 *
 * @method getIsActive()
 * @method setIsActive()
 * @method getShippingMethodCode()
 * @method setShippingMethodName()
 * @method getShippingPrice()
 * @method setShippingPrice()
 *
 */
class Hobox_Integratorshipping_Model_Shipping_Configuration extends Mage_Core_Model_Abstract {

}