<?php

/**
 * @category   Hobox
 * @package    Hobox_Integratorapi
 * @author     Rogeres Nascimento
 * @company    Hobox
 * @copyright (c) 2017, Hobox
 *
 *
  */

class Hobox_Integratorpayment_Model_Payment extends Mage_Payment_Model_Method_Abstract {

    protected $_code = 'hobox_payment';

    protected $_isInitializeNeeded      = true;

    /**
     * Can use this payment method in administration panel?
     */
    protected $_canUseInternal          = true;

    /**
     * Is this payment method suitable for multi-shipping checkout?
     */
    protected $_canUseForMultishipping  = false;

     /**
     * Can show this payment method as an option on checkout payment page?
     */
    protected $_canUseCheckout          = false;

    /**
     * Can capture funds online?
     */
    protected $_canCapture              = true;


   /*
    * Method added just to be compatible with the Adyen Payment module which has
    * an observer that calls this method when the order is canceled.
    *
    * For more info see:
    *  https://github.com/Adyen/magento/commit/05adaf0e53eb2abbf0f29c5a93221f77431e462d
    *  http://answers.magentocommerce.com/answers/4643-en_us/product/22803/adyen-adyen-payment-questions-answers/questions.htm?sort=helpfula
    *
    */
    public function SendCancelOrRefund($param1 = nil, $param2 = nil, $param3 = nil, $param4 = nil) {

    }
}

